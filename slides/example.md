# Diaporama 100% Markdown

---

## Introduction:
### Comment naviguer dans le diaporama

> 1. → et ← : déplacement horizontal 
> 2. ↓ et ↑ : déplacement vertical 
> 3. Echap ou O: aperçu de la présentation   
> 4. B: mettre en pause la présentation
> 5. S : mode présentation
> 6. F: mode plein écran (Echap pour sortir du mode plein écran) 

Note: pas d'indication de background dans le markdown → celui paramétré dans index.html

---

## Chapitre 1: Le Markdown externe
### Le fichier index.html
<!-- .slide: data-background="#cfe2f3" -->

Dans le fichier **index.html** on crée une balise `<section> </section>` dans laquelle:

* On fait appel au fichier **example.md** qui contient le contenu des diapos
* On paramètre les caractères:
	* qui serviront à passer d'une diapo à l'autre (de gauche à droite Ou de haut en bas),
	* qui serviront à ajouter des notes (uniquement visibles en mode présentateur)

--

## Chapitre 1: Le Markdown externe
### Le fichier example.md
<!-- .slide: data-background="#cfe2f3" -->

Dans le fichier **example.md**  on peut utiliser la syntaxe ***Markdown***.
En outre il faudra:

* Annoncer le changement de diapo avec la chaîne de carcatère choisie (ici 3 tirets encadrés par 1 saut de ligne au dessus et au dessus pour passer à la diapo suivante à droite & même chose avec 2 tirets pour passer à la diapo suivante en dessous) <!-- .element: class="fragment" data-fragment-index="1" -->
* éventuellement ajouter des paramètres à l'aide de commentaires significatifs: <!-- .element: class="fragment" data-fragment-index="2" -->
  
  * ainsi pour changer la couleur de fond de cette diapo en bleu on ajoutera:
  
```css
  <!-- .slide: data-background="#cfe2f3" -->
```

  * et pour qu'une liste s'affiche petit à petit (fragments) on ajoutera en dessous de chaque * item la ligne:

```css
<!-- .element: class="fragment" data-fragment-index="1" -->
```

Note: This will only appear in the speaker notes window.

---

## Chapitre 2: Écrire en markdown
### Titres et emphases
On peut évidemment utiliser les niveaux de titres:

```markdown
### Titre de niveau 3
#### Titre de niveau 4 
```
donne:
### Titre de niveau 3
#### Titre de niveau 4

Et les emphases:

```markdown
**gras**
*italique*
~~barré~~
```
donne:

**gras**

*italique*

~~barré~~

--

## Chapitre 2: Écrire en markdown
### Mise en forme

On peut utiliser les **listes**:

* item 1
* item 2
	* item 2.1
	* item 2.2
		* item 2.2.1
* item 3

1. premier point
2. deuxième point



Mais aussi les citations:

> pour faire un aparté par exemple ... dans lequel on pourrait dire qu'il faut faire attention au saut de lignes obligatoires pour les listes ou faire revenir la ligne suivante en dessous...

--

## Chapitre 2: Écrire en markdown
### Mise en forme 2: test de sauts de lignes

Voici une ligne pour démarrer.





Cette ligne est séparée de celle du dessus par **deux** nouvelles lignes, aussi ce sera un *paragraphe séparé*. 





Cette ligne est aussi un paragraphe séparé (2 lignes), mais...

Cette ligne n'est séparée que par **une seule nouvelle ligne**, aussi c'est une ligne séparée dans le *même paragraphe*. Et ce début de phrase commance par un simple retour à la ligne → résultat: il reste dans la continuité)..

---

## Chapitre 2: Écrire en markdown

### Du texte augmenté

avec des images ![cc](images/CC.png) insérées dans le texte 

>  → moche et encadré : regarder le css ?

avec des liens:

* de base: https://gitlab.com/archeomatic/slydes
* avec un mot à cliquer: [lien1](https://github.com/hakimel/reveal.js/wiki/Example-Presentations)
* Et même une image à cliquer: [![gitlab](images/gitlab.png)](https://gitlab.com/archeomatic/slydes)

--

## Chapitre 2: Écrire en markdown
###  Des images

<!-- .slide: data-background="#ffffff" -->

On re-change de couleur de fond (blanc).

et on en profite pour afficher une image seule...

![test](images/cover.png)

centrée en h et en v? 

--

## Chapitre 2: Écrire en markdown
###  De la vidéo ?

<!-- .slide: data-background="#ffffff" -->
En Markdown **Pur**:

[![TEXTE ALT IMAGE ICI](http://img.youtube.com/vi/Q_3eJ2FxvZc/0.jpg)](http://www.youtube.com/watch?v=Q_3eJ2FxvZc)

> fonctionne mais s'ouvre dans le même onglet

--

## Chapitre 2: Écrire en markdown
###  De la vidéo ?

<!-- .slide: data-background="#ffffff" -->
En HTML:

<a href="http://www.youtube.com/watch?feature=player_embedded&v=Q_3eJ2FxvZc" target="_blank"><img src="http://img.youtube.com/vi/Q_3eJ2FxvZc/0.jpg" 
alt='TEXTE ALT IMAGE ICI' width='240' height='180' border='10' /></a>

> fonctionne avec une vignette plus petite et la vidéo qui  s'ouvre dans un nouvel onglet ☺☻♥

--

## Chapitre 2: Écrire en markdown...
###  Du code

On peut facilement insérer un bloc de code sql:
```sql
select *
from table as toto
where "champ" = 'valeur'
```
ou encore avec le langage R:
```R
monobjet <- mean(matable$mavariable)
```

ou dans le texte `ceci est du code`par exemple.

> reconnaissance du langage → OK

---

## Chapitre 3: ... les limites
### certains trucs fonctionnent moins bien
<!-- .slide: data-background="#E41B17" -->

* Les emojis ne fonctionnent pas  :warning:  :banana:
<!-- .element: class="fragment" data-fragment-index="1" -->
→ plugin necessaire? 
<!-- .element: class="fragment" data-fragment-index="1" -->

* Les caracteres **ascii** fonctionnent ! alt + 26 → alt + 1☺ alt + 3 ♥  alt +16 ►  
<!-- .element: class="fragment" data-fragment-index="2" -->

* Les tableaux ... un peu <!-- .element: class="fragment" data-fragment-index="3" -->

|A|B|C|
|--|:--:|--:|
|2|99|anticonstitutionnellement|
|0012|2569898|opo|
<!-- .element: class="fragment" data-fragment-index="3" -->

---

## Chapitre 4: On va plus loin ?
### La couleur et les  images de fond

<!-- .slide: data-background="#E41B17" -->
Changement de couleur par un proto  Rouge-Inrap (code html = #E41B17), en insérant sous le titre de la diapo le commentaire signifiant:

```css
<!-- .slide: data-background="#E41B17" -->
```

Aussi on peut changer l'image de fond:

* copier une image de 2116 x 1190 px dans le dossier images/backgrounds

* faire appel à cette image en insérant le commentaire signifiant:

```css
<!-- .slide: data-background="images/backgrounds/monimage.png" -->
```

> Ne s'applique qu'à la diapo en cours, si pas de précision il utilise le fond paramétré dans le **index.html**

--

## Chapitre 4: On va plus loin ?
### Le css 

D'abord on peut changer le theme par défaut dans e fichier **index.html**, en modifiant la ligne:

```css
<!-- theme utilisé (voir dans css/theme) default/beige/blood/moon/night/serif/simple/sky/solarized -->
<link rel="stylesheet" href="css/theme/default.css" id="theme">
```

--

## Chapitre 4: On va plus loin ?
### Le css 

Les styles sont contenus dans le fichier  **reveal.css** (dossier [css]) et sa version minimisée **reveal.min.css**



> L'idéal avant de bricoler:
>
> * est d'en faire un copié-collé, le renommer **slydes_reveal.css**
> * puis dans le fichier **index.html** de modifier la ligne qui déclare la feuille de style à utiliser en:
> ```css
> <link rel="stylesheet" href="css/slydes_reveal.css">
> ```
