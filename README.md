Mon 1er test de slides propulsé par gitlab
======================================
## Avant-propos

Ce projet est basé à partir d'un clone de celui-ci: https://gitlab.com/eschabell/beginners-guide-automated-workshops.git

le tutoriel [Eng] est disponible en ligne: https://opensource.com/article/20/4/create-web-tutorial-git

## Introduction

Ce projet **slydes** est un test d'automatisation de mise en ligne de diaporamas à l'aide du *framework* de présentation html [reveal.js](https://revealjs.com/) [^1] et le système d'intégration continue de [gitlab.com](https://gitlab.com/).

[^1]: le framework *reveal.js* est celui utilisé par [slides.com](https://slides.com/) du même créateur @hakimel

L'objectif serait de disposer d'un modèle avec un plan de base, un modèle de fond standard et des exemples de diapositives pour aider ceux que cela intéresse à le développer eux-même.. enfin moi d'abord ! 

L'intérêt est multiple:

* grâce au sytème **git** **plusieurs collaborateurs peuvent travailler sur le même projet** avec un système de clone du dossier du projet en local et une version "toujours à jour" dans le dépôt gitlab.

* grâce au sytème d'intégration continue **chaque changement envoyés sur le dépôt (= chaque *commit* ) génére automatiquement une nouvelle version en ligne du diaporama**.

Ce projet peut servir d'exemple et de preuve de concept pour un éventuel déploiement pour par exemple mettre en lifgne des diaporamas pour des formations ... dont les uspports sont déjà propulsés sur le web et en PDF par un système équivalent: https://formationsig.gitlab.io/toc/.


[![Cover Slide](images/cover.png)](https://eschabell.gitlab.io/beginners-guide-automated-workshops)

## Hébergements

Le projet **slydes** est hébergé sur Gitlab à l'adresse:
https://gitlab.com/archeomatic/slydes

Normalement, le diaporama sera publié à l'adresse:
https://archeomatic.gitlab.io/slydes

## Contenu du projet Gitlab

Le dossier Gitlab du projet **slydes** contient:

* ce fichier **README.md**

* un fichier **cover.png** de 2778x1486 px

* un fichier **.gitlab-ci.yml** contenant un minimum de code:

```
  pages:
    stage: deploy
    script:
      - mkdir .public
      - cp -r slides/* .public
      - mv .public public
    artifacts:
      paths:
        - public
    only:
      - master
```

* un fichier **.gitignore** qui sert à definir les fichier à ignorer en cas de `git push*, enfin je crois..

* un dossier **slides** contenant lui-même des sous-dossiers:

  * **css**
  * **images**
  * **js**
  * **lib**
  * **plugin**
  * **test**

  et des fichiers:

  * *Gruntfile.js* ???
  * *index.html* contenant la présentation
  * *LICENSE*
  * *package.json* probablement un fichier de configuration (à l'instr de *book.json* pour gitbook)

## Options et plugins

### export PDF ? 

Pour exporter une présentation reveal.js, il y a deux possibilités :
*  Utiliser **?print-pdf** , natif à reveal.js, résultat plutôt moyen voire carrément non fonctionnel.
  

→ si on tape l'adresse **avec le suffixe** `?print-pdf`, par exemple: https://archeomatic.gitlab.io/slydes/?print-pdf#/ **on obtient une version imprimable en pdf** (attention elle n'est pas téléchargée, il faut ensuite choisir imprimer depuis le navigateur web)
    
    :warning: On préférera l'explorateur Chrome → [Ctrl]+P → format paysage → pas de marge → cocher "Graphiques d'arrière plan"

*  Utiliser decktape , bien plus puissant
     → [https://github.com/astefanutti/decktape ](https://github.com/astefanutti/decktape)

    explications en français : https://www.marknotes.fr/docs/Development/reveal.html#1-2-decktape

## Les questions ?

En vrac...

* Comment faire une présentation reveal.js avec un minimum de code ? existe-t'il des logiciels pour cela ?
* Peut-on faire du *broadcasting* (diffusion en live du diaporama contrôlée par le présentateur) ? Note: c'est l'avantage majeur de slides.com du même auteur
* Cela fonctionne sur gitlab.com. Est-ce que cela pourrait fonctionner sur le gitlab.inrap.fr ?   → Y'a pas de raison
* Peut-t'on récupérer des diaporama effectués avec slides.com ? (à partir de l"export html -version payante- par exemple)

→ 1er test = copié-collé à l'arrache depuis un export de slides.com cad du fichier index.html, du dossier [lib] et du dossier [stat1-4] :arrow_forward: ca fonctionne FARPAITEMENT ! par contre le code html est trop bordelique pour être repris simplement.... 

* Et l'inverse ? 
* Convertir un [Powerpoint en reveal.js](https://www.marknotes.fr/docs/Development/reveal.html#3-convertir-une-presentation-powerpoint-en-reveal-js) ? 
* Des [plugins sympa](https://www.marknotes.fr/docs/Development/reveal.html#4-plugins-sympas) → vérifier s'ils fonctionnent avec gitlab

## Ressources
* le site officiel [reveal.js](https://revealjs.com/) et le [dépôt git](https://github.com/hakimel/reveal.js/) 
* le tuto dont cette page s'est inspiré au départ sur [opensource.com](https://opensource.com/article/20/4/create-web-tutorial-git)
* un exemple html avec [utilisation du markdown](https://gitlab.telecom-paris.fr/sen/docs_template/blob/dab560116dd19461a8b024141fded3e27c75091c/reveal.js/plugin/markdown/example.html) et d'autres trucs intéressant sur reveal.js
* [liste de plugins](https://github.com/hakimel/reveal.js/wiki/Plugins,-Tools-and-Hardware) et outils pour reveal.js
* un petit [*cheatsheet* sur markdown et reveal.js](https://www.devopsschool.com/blog/reveal-js-markdown-cheatsheet-complete-guide/)
* un [tuto](http://vishalgupta.me/md-slides/) pour faire des slides en md avec reveal.js